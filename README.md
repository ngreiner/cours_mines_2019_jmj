# README #

Cette boîte de dépôts contient des transcriptions des vidéos des cours dispensés par Jean-Marc JANCOVICI à MINES ParisTech durant l'année universitaire 2018-2019 sous forme de documents PDF.

Ces documents ont été réalisés par les membres de l'association de loi 1901 _The Shifters_ au cours des second semestre 2019 et premier semestre 2020.

Ces transcriptions ont été réalisées dans le but d'être les plus fidèles possibles aux propos tenus par l'orateur lors de ses interventions. Cependant, comme toute transcription écrite de propos tenus oralement, celles-ci sont nécessairement imparfaites, et certains éléments du discours original ont été supprimés ou révisés -- quoique de façon marginale -- afin d'obtenir un texte plus fluide.

Les interventions filmées originales de Jean-Marc JANCOVICI sont accessibles via l'URL suivante :
[https://www.youtube.com/watch?v=xgy0rW0oaFI&list=PL1Rgjj9-zLqcJRa_eOPHdNwi_pX9rpYYx](https://www.youtube.com/watch?v=xgy0rW0oaFI&list=PL1Rgjj9-zLqcJRa_eOPHdNwi_pX9rpYYx)

### Téléchargement ###

Pour télécharger le contenu de cette boîte de dépôts, il vous suffit de cliquer sur «Downloads» dans la colonne ci-contre (à gauche), puis sur «Download repository» au centre de la page ainsi actualisée.

Alternativement, si vous utilisez `git`, vous pouvez taper la commande suivante dans un terminal :

	git clone https://ngreiner@bitbucket.org/ngreiner/cours_mines_2019_jmj.git

### License ###

Les documents de cette boîte de dépôts sont placés sous license CC-BY-NC-SA. Ils sont librement distribuables, sauf à des fins commerciales. Dans le cas où ces documents serviraient à produire des créations dérivées, il convient aux auteurs de ces créations dérivées de faire mention de la provenance des présents documents et de placer ces créations dérivées également sous licence CC-BY-NC-SA.

### Contributeurs ###

Merci à Pascal ESPOSITO, Alix LAGET, Laure BOURGOIN, Patrick STEKELOROM, Florian NGUYEN, Marie-Amélie AUVINET, François MAITRE, Michel EDIN, Philippe CLAUDE, Matthis HAMY, Amandine GODET, Charles GAY, Nathan GREINER, Florence CHARLIER, Pierre GAYTE, Lionel LEMAIRE, Tom DOMENGE, et tous ceux qui ont participé, par leurs petites et grandes contributions, à la production de ces documents.

### Contact ###

Pour toute question, suggestion ou notification (notamment de coquilles persistentes dans les documents), merci d'adresser celles-ci à : [nathan.greiner@m4x.org](mailto:nathan.greiner@m4x.org?subject=[Bitbucket-Shifters]%20Transcriptions%20cours%20JMJ : )